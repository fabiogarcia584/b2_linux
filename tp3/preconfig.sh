#!/bin/bash
# install script for Apache/mod_ssl/certbot/netdata and firewalld
# Fabio - 2/11/2021

# mise à jour des paquets déjà installés 
dnf upgrade

# Installation de firewalld et création des zones firewall

dnf install -y firewalld
systemctl enable --now firewalld
firewall-cmd --set-default-zone=drop
firewall-cmd --zone=drop --add-interface=inet
firewall-cmd --new-zone=ssh --permanent
firewall-cmd --zone=ssh --add-port=22/tcp --permanent
firewall-cmd --new-zone=clients --permanent
firewall-cmd --zone=clients --add-port=443/tcp --permanent
firewall-cmd --reload

# Installation netdata et config firewall

bash <(curl -Ss https://my-netdata.io/kickstart.sh)
firewall-cmd --permanent --add-port=19999/tcp
firewall-cmd --reload

# Installation Apache 
dnf install -y httpd
systemctl enable --now httpd

# Installation mod_ssl/certbot

dnf install -y epel-release
dnf install -y snapd
systemctl enable snapd.socket
systemctl start snapd.socket
ln -s /var/lib/snapd/snap /snap
snap install --classic certbot
dnf install -y mod_ssl

touch /etc/httpd/conf.d/virtual.conf

sed "<VirtualHost *:80>
    DocumentRoot "/www/html" #on mettra un site à la fin donc pour l'instant on va rester sur la page d'accueil
    ServerName elbaronitosamedi.website

</VirtualHost>" /etc/httpd/conf.d/virtual.conf
systemctl restart httpd

# Obtention https

 certbot --apache
