# TP3 : Your own shiet

# Intro

Pour ce dernier TP j'ai choisi de monter ma propre solution d'hébergement web à l'aide d'un VPS dans le but de pouvoir appliquer de façon concrète ce qui a été vu lors des TP précedents.

Pour ce TP j'utiliserais un VPS de chez OVH et j'utiliserais la distribution centos 8.


# Sommaire

- [TP3 : Your own shiet](#TP3-:-Your-own-shiet)
- [Sommaire](#sommaire)
- [I. Configuration de base du VPS](#i-Configuration-de-base-du-VPS)
  - [A.Le mot de passe](#A.Le-mot-de-passe)
  - [B.Le firewall](#B.Le-firewall)
  - [C.Les clés SSH](#C.Les-clés-SSH)
  - [D.Le nom de domaine](#D.Le-nom-de-domaine)
- [II. Mise en place du serveur web](#Mise-en-place-du-serveur-web)
- [III. Maintien en conditions opérationnelles](#iii-Maintien-en-conditions-opérationnelles)
  - [A. Le monitoring](#a-Le-monitoring)
  - [B. Les zones firewall](#b-Les-zones-firewall)
  - [C. HTTPS](#c-HTTPS)
- [IV. Le script d'automatisation](#iv-Le-script-d'automatisation)




# I. Configuration de base du VPS 

Dans cette partie je vais juste détailler les premières manipulations à faire après la première connexion au VPS:

Commme j'ai choisi OVH comme fournisseur je n'ai pas eu à installer d'OS, seulement à choisir parmis la liste de distributions fournies, de plus étant chez un hébergeur, certaines choses sont déjà pré-configurées (adresse IP,DNS,SSH entre autre), je ne rentrrais donc pas dans es  détails de la configuration de ces choses là dans cette documentation.

## A.Le mot de passe

On commence donc par se connecter en SSH au VPS avec les informations fournies par mail :
```bash=
PS C:\Users\Fabio> ssh centos@141.94.68.163
centos@141.94.68.163's password:
Activate the web console with: systemctl enable --now cockpit.socket
```


Une fois celà fait la pemière chose à faire ABSOLUEMENT est de changer le mot de passe, vous devez être le seul à le connaître : 
```bash=
[centos@vps-3337253e ~]$ sudo passwd
Changing password for user root.
New password:
Retype new password:
passwd: all authentication tokens updated successfully.
```

## B.Le firewall

Ensuite on va s'occuper du `firewall`, dans mon cas je vais me servir de `firewalld` car c'est celui que je connais le mieux car utilisé en cours : 

On commence par l'installer et faire en sorte qu'il démarre au lancement de la machine : 
```bash=
[centos@vps-3337253e ~]$ sudo dnf -y install firewalld #permet d'installer firewalld
Last metadata expiration check: 3:14:09 ago on Thu 28 Oct 2021 09:58:41 AM UTC.
Dependencies resolved.
<...>
Complete!
[centos@vps-3337253e ~]$ sudo systemctl enable --now firewalld #permet l'activation au démarrage de la machine de firewalld, --now permet de up le service en même temps
[centos@vps-3337253e ~]$ sudo systemctl status --now firewalld #permet de vérifier si le service et actif
● firewalld.service - firewalld - dynamic firewall daemon
   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)
   Active: active (running) since Thu 2021-10-28 13:14:10 UTC; 10s ago
     Docs: man:firewalld(1)
 Main PID: 13931 (firewalld)
    Tasks: 2 (limit: 10910)
   Memory: 24.4M
   CGroup: /system.slice/firewalld.service
           └─13931 /usr/libexec/platform-python -s /usr/sbin/firewalld --nofork --nopid

Oct 28 13:14:10 web systemd[1]: Starting firewalld - dynamic firewall daemon...
Oct 28 13:14:10 web systemd[1]: Started firewalld - dynamic firewall daemon.
Oct 28 13:14:10 web firewalld[13931]: WARNING: AllowZoneDrifting is enabled. This is considered an insecure configuration option. It will be removed in a future release. Please consider disabling it now.
[centos@vps-3337253e ~]$
```
Une fois celà fait nous allons pouvoir le configurer, pour l'instant nous n'autoriserons que le trafic sur le port 22 en TCP pour garder notre connexion SSH à la machine : 

On commence par lister une première fois les ports et services autorisés sur notre machine à l'aide de `sudo firewall-cmd --list-all`

```bash=
[centos@vps-3337253e ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: eth0
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

On peut ensuite faire le ménage à l'aide de `sudo firewall-cmd --remove-service` pour arriver à ce résultat là : 

PS : ne pas oublier de recharger le fichier de config à l'aide de `sudo firewall-cmd --reload` pour que les changements soient pris en compte.

```bash=
[centos@vps-3337253e ~]$ sudo firewall-cmd --remove-service=dhcpv6-client --permanent #on retire dhcpv6-client de la liste des services autorisés
success
[centos@vps-3337253e ~]$ sudo firewall-cmd --remove-service=cockpit --permanent #pareil mais avec cockpit cette fois-ci
success
[centos@vps-3337253e ~]$ sudo firewall-cmd --reload # on recharge la config pour que les changements soient pris en compte
success
[centos@vps-3337253e ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: eth0
  sources:
  services: ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

## C.Les clés SSH

Pour finir avec les configurations préalables nous allons s'occuper d'une chose moins "essentielle" mais qui reste une bonne pratique et une habitude à avoir: 

On change le hostname de la machine, histoire de pouvoir se repérer plus facilement : 
```bash=
[centos@vps-3337253e ~]$ sudo hostname web #permet de changer directement le hostname
[centos@vps-3337253e ~]$ sudo vi /etc/hostname #permet de garder le changement de façon durable
[centos@vps-3337253e ~]$ hostname
web
```
Après reconnection on peut voir que les changements sont bien pris en compte : 
```bash=
PS C:\Users\Fabio> ssh centos@141.94.68.163
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Thu Oct 28 14:59:34 2021 from 193.250.68.214
[centos@web ~]$
```

## D.Le nom de domaine (optionnel)

Pourquoi je dis que c'est optionnel ? En fait là je parle pas du nom de domaine en lui-même quand je dis que c'est optionnel mais du fait de le faire maintenant, le truc c'est que là je vais vous montrez comment relier votre nom de domaine à votre serveur et entre le moment où vous allez faire la manipulation et le moment où votre site sera réellement accessible il peut se passer jusqu'à 48 heures et pour certains celà pourrait être génant donc je préfère vous le montrer maintenant. 

Du coup, pour lier pour votre nom de domaine à votre serveur il vous faut... un nom dedomaine ça paraît logique, à savoir qu'un nom de domaine ça se loue à l'année au près d'un fournisseur et je ne rentrerais pas plus dans le détail de comment obtenir un nom de domaine car une recherche google on tombe sur un site du servie public fournissant une liste de fournisseur certifié ainsi que 3 fournisseurs différents dans les premiers résultats de recherche.

Donc une fois que vous avez votre nom de domaine il va vous suffir d'aller dans le menu configuration de votre nom de domaine, une fois celà fait vous allerdans "Enregistrement DNS"
 puis vous modifier l'enregistrement de type "A", vous remplacer l'adresse IP présente par celle de votre serveur et c'est tout, il ne reste plus qu'à attendre que les modifications soient transmises au reste du monde.


# II. Mise en place du serveur web 

Dans cette partie je vais décrire la mise en place notre serveur web, cela va comprendre donc l'installation et une configuration sommaire du serveur pour l'instant, le but étant qu'à la fin on aura plus qu'à venir déposer notre site pour qu'il soit accessible avec une requête `curl`. 

On commence par installer notre application, ici on utilisera `Apache` : 

```bash=
sudo dnf -y install httpd #commmande d'installation de paquet sur centos
Last metadata expiration check: 1:59:18 ago on Fri 05 Nov 2021 01:51:02 PM UTC.
Dependencies resolved.
<...>
Complete!
[centos@web ~]$
```

Maintenant que l'installation a été effectué place à la configuration : 


On commence par lancé le service et faire en sorte qu'il démarre au lancement de la machine : 
```bash=
[centos@web ~]$ sudo systemctl start httpd
[centos@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
```

Ensuite on ouvre le port utilisé par Apache pour communiquer avec l'extérieur, par défaut celui-ci est le 80 : 
```bash=
[centos@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[centos@web ~]$  sudo firewall-cmd --reload
success
```

On peut enfin tester si notre service fonctionne : 

```bash=
[centos@web ~]$  sudo systemctl is-active httpd
active
[centos@web ~]$ sudo systemctl is-enabled httpd
enabled
[centos@web ~]$  curl localhost
<!DOCTYPE html>
<html lang="en">
<head>
[...]
```

Normalement à partir de là nous pouvons aussi lancer des requêtes au serveur depuis notre poste de travail au serveur web : 

```bash=
PS C:\Users\Fabio> curl 141.94.68.163
curl : <!DOCTYPE html>
<html lang="en">
<head>
```


# III. Maintien en conditions opérationnelles


Alors dans les faits notre serveur web fonctionne déjà, il est même déjà accessible via un navigateur web, cependant, pour que notre installation puisse tenir dans le temps,il va falloir encore faire quelques manipulations. Nous allons donc mettre en place dans cette partie un système de monitoring et de backup automatisées. 


## A. Le monitoring 

Pour effectuer du monitoring sur notre serveur on va se servir de Netdata car c'est une solution simple, complète, lègère, performante et qui est compatibles avec énormément tout ce qui existe.

On commence donc par l'installation : 

```bash=
[centos@web ~]$ sudo su - #Ici on va être obligé de passer en root pour installer Netdata
Last failed login: Mon Nov  8 21:04:48 UTC 2021 from 221.131.165.65 on ssh:notty
There were 22229 failed login attempts since the last successful login.
[root@web ~]# bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
<...>
[root@web ~]# exit
logout
[centos@web ~]$
```

On met à jour la configuration du firewall : 

```bash=
[centos@web ~]$ sudo firewall-cmd --permanent --add-port=19999/tcp
success
[centos@web ~]$ sudo firewall-cmd --reload
success
```


## B. Les zones firewall

Pour renforcer la sécurité au niveau de l'accés à notre serveur, on va déclarer des zones firewall, celà va permttre de filtrer plus précisément les entrées et sorties d'informations sur notre serveur : 

```bash=
[centos@web ~]$ sudo firewall-cmd --set-default-zone=drop
[sudo] password for samedi:
success
[centos@web ~]$ sudo firewall-cmd --zone=drop --add-interface=inet
success
[centos@web ~]$ sudo firewall-cmd --new-zone=ssh --permanent
success
[centos@web ~]$ sudo firewall-cmd --zone=ssh --add-source=193.250.68.214 --permanent
success
[centos@web ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
success
[centos@web ~]$ sudo firewall-cmd --new-zone=clients --permanent
success
[centos@web ~]$ sudo firewall-cmd --zone=clients --add-port=443/tcp --permanent #On ajoute directement le port 443 et pas le 80 car juste après je vais mettre le site en HTTPS 
success
[centos@web ~]$ sudo firewall-cmd --reload
success
[centos@web ~]$
```

Grâce à ça les seules connexions autorisées sur le serveur seront les requêtes sur le port 80 et mes connexions ssh depuis chez moi pour quand j'aurais besoin d'intervenir sur le serveur. 

## C. HTTPS

Dans cette partie je Vais vous montrer comment obtenir un certificat SSL et faire ne sorte que votre site soit accessible en HTTPS:
Pour celà, on se servira de `certbot`, un paquet disponnible sous Linux permettant d'automatiser une grosse partie du processus et ce entièrement gratuitement car il se sert de certificats Let's Encrypt, l'inconvéniant étant le fait que ces certficats ne garantissent pas la meilleur sécurité, cependant pour ce que nous allons faire su site celà amplement suffisant. 

Pour commencer, no va installer le dépot dans lequel se trouve le paquet `cerbot` : 

```bash=
[centos@web ~]$ sudo dnf install -y epel-release
<...>
[centos@web ~]$ sudo dnf install -y snapd
<...>
[centos@web ~]$ sudo systemctl enable --now snapd.socket
[centos@web ~]$ sudo systemctl start snapd.socket      #théoriquement le --now était censé démarrer le processus mais apparement dans                                                         mon cas ça n'a pas voulu démarrer 
[centos@web ~]$ sudo ln -s /var/lib/snapd/snap /snap
```

A partir de là on va pouvoir installer `certbot`

```bash=
[centos@web ~]$ sudo snap install --classic certbot
<...>
```

Certbot permettant la gestion de certificats pour plusieurs sites en même temps sur une même machine, il se sert de virtual hosts pour pouvoir différencier les sites et les rattacher aux bons certificats, on va donc devoir au préable rattacher notre site à un virtual host, il faudra aussi installer `mod_ssl` pour qu'Apache puisse gérer les certificats SSL : 

```bash=
[centos@web ~]$ sudo dnf install -y mod_ssl
<...>
[centos@web ~]$ sudo vi /etc/httpd/conf.d/virtual.conf
[centos@web ~]$ cat /etc/httpd/conf.d/virtual.conf
<VirtualHost *:80>
    DocumentRoot "/www/html" #on mettra un site à la fin donc pour l'instant on va rester sur la page d'accueil
    ServerName elbaronitosamedi.website

</VirtualHost>
[centos@web ~]$ sudo systemctl restart httpd
```

Et enfin on va pouvoir laisser `Certbot` faire son travail : 

```bash=
[centos@web ~]$ sudo certbot --apache
Saving debug log to /var/log/letsencrypt/letsencrypt.log

Which names would you like to activate HTTPS for?
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
1: elbaronitosamedi.website
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Select the appropriate numbers separated by commas and/or spaces, or leave input
blank to select all options shown (Enter 'c' to cancel):
Requesting a certificate for elbaronitosamedi.website

Successfully received certificate.
Certificate is saved at: /etc/letsencrypt/live/elbaronitosamedi.website/fullchain.pem
Key is saved at:         /etc/letsencrypt/live/elbaronitosamedi.website/privkey.pem
This certificate expires on 2022-02-11.
These files will be updated when the certificate renews.
Certbot has set up a scheduled task to automatically renew this certificate in the background.

Deploying certificate
Successfully deployed certificate for elbaronitosamedi.website to /etc/httpd/conf.d/virtual-le-ssl.conf
Congratulations! You have successfully enabled HTTPS on https://elbaronitosamedi.website

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
If you like Certbot, please consider supporting our work by:
 * Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
 * Donating to EFF:                    https://eff.org/donate-le
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
[centos@web ~]$
```

Lors de la manipulation vous aurez quelques question de sécurité auxquelles vous devrez répondre mais rien de bien méchant, par contre dans le lot il y a une demande de collecte de données donc faites attentionn avant de répondre. 

Enfin, vous n'avez plus qu'à aller sur vote site depuis votre navigateur et admirer votre magnifique connexion sécurisée. 


# IV. Le script d'automatisation 

Et pour finir voici un script qui permert d'installer tout ce que j'ai présenté précédément et qui permet d'avoir directement une connexion https sur son site : [preconfig.sh](/preconfig.sh)

On pourra donc arriver à quelque chose qui ressemble à ça https://elbaronitosamedi.website/ 
