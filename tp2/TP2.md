# TP2 pt. 1 : Gestion de service

# Sommaire

- [TP2 pt. 1 : Gestion de service](#tp2-pt-1--gestion-de-service)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [1. Intro](#1-intro)
  - [2. Setup](#2-setup)
    - [A. Serveur Web et NextCloud](#a-serveur-web-et-nextcloud)
    - [B. Base de données](#b-base-de-données)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)


# I. Un premier serveur web

## 1. Installation

Installation de `httpd` : 

```
[samedi@web ~]$ sudo dnf install -y httpd
```

Démarrer `Apache` : 

```
[samedi@web ~]$ systemctl start httpd
```

Faire en sorte qu'il démarre automatiquement : 

```
[samedi@web ~]$ systemctl enable httpd
```

Commande `ss` : 

```
[samedi@web ~]$ sudo ss -alnpt | grep httpd
LISTEN 0      128                *:80              *:*    users:(("httpd",pid=944,fd=4),("httpd",pid=942,fd=4),("httpd",pid=941,fd=4),("httpd",pid=912,fd=4))
```

Ouverture du port 80 : 

```
[samedi@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
[sudo] password for samedi:
success
[samedi@web ~]$ sudo firewall-cmd --reload
success
```

Tests : 

Service démarré : 

```
[samedi@web ~]$ systemctl status httpd
   httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-10-06 09:08:02 CEST; 29min ago
     Docs: man:httpd.service(8)
```

service configuré pour démmarrer automatiquement : 

```
[samedi@web ~]$ systemctl is-enabled httpd
enabled
```

Vérifier si on peut joindre le serveur localement : 

```
[samedi@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <...>
```

## 2. Avancer vers la maîtrise du service

Pour activer démarrage automatique d'Apache : 

```
systemctl enable httpd
```

Pour savoir si le service démarre automatiquement : 

```
[samedi@web ~]$ systemctl is-enabled httpd
enabled
```

Affichage de `httpd.service` : 

```
[samedi@web etc]$ sudo cat /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
```

Déterminer sous quel utilisateur tourne le processus Apache : 

```
[samedi@web etc]$ sudo cat /etc/httpd/conf/httpd.conf | grep User
User apache
<...>
```

Confirmation de l'utilisateur sous lequel tourne Apache : 

```
[samedi@web etc]$ ps -ef | grep httpd
root         912       1  0 09:08 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       940     912  0 09:08 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       941     912  0 09:08 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       942     912  0 09:08 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       944     912  0 09:08 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1895     912  0 09:44 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```

vérification des droits : 

```
[samedi@web www]$ ls -al
total 4
drwxr-xr-x.  4 root root   33 Sep 29 16:53 .
drwxr-xr-x. 22 root root 4096 Sep 29 16:53 ..
drwxr-xr-x.  2 root root    6 Jun 11 17:35 cgi-bin
drwxr-xr-x.  2 root root    6 Jun 11 17:35 html
```

Changer l'utilisateur utilisé par Apache : 

Création du nouveau user : 

```
[samedi@web /]$ sudo useradd newapache -m -s /sbin/nologin -u 1004
[sudo] password for samedi:
[samedi@web /]$ sudo passwd newapache
Changing password for user newapache.
New password:
BAD PASSWORD: The password is a palindrome
Retype new password:
passwd: all authentication tokens updated successfully.
```

Ajout au group Apache : 


```
[samedi@web /]$ sudo usermod -aG apache newapache
[samedi@web /]$
```

Modification de la config de Apache : 
```
[samedi@web /]$ sudo nano /etc/httpd/conf/httpd.conf
[samedi@web /]$ [samedi@web /]$
[samedi@web /]$ sudo cat /etc/httpd/conf/httpd.conf | grep User
User newapache
```

Après avoir restart Apache : 

```
==== AUTHENTICATION COMPLETE ====
[samedi@web /]$ ps -ef | grep httpd
root        2146       1  0 11:29 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newapac+    2147    2146  0 11:29 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newapac+    2148    2146  0 11:29 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newapac+    2149    2146  0 11:29 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newapac+    2150    2146  0 11:29 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```

Faites en sorte que Apache tourne sur un autre port (port 1234) : 

Modification de la config : 
```
[samedi@web /]$ sudo nano /etc/httpd/conf/httpd.conf
[samedi@web /]$ [samedi@web /]$ sudo cat /etc/httpd/conf/httpd.conf | grep Listen
Listen 1234
```

Modification du firewall : 
```
[samedi@web /]$ sudo firewall-cmd --add-port=1234/tcp --permanent
success
[samedi@web /]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
```

Après avoir relancé Apache : 

```
[samedi@web /]$ sudo ss -alnpt | grep httpd
LISTEN 0      128                *:1234            *:*    users:(("httpd",pid=2434,fd=4),("httpd",pid=2433,fd=4),("http
",pid=2432,fd=4),("httpd",pid=2430,fd=4))
```

```
[samedi@web /]$ curl localhost:1234
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <...>
```

[Fichier de configuration](./conf/httpd.conf)

# II. Une stack web plus avancée


## 2. Setup

### A. Serveur Web et NextCloud

```
[samedi@web ~]$ sudo dnf install epel-release
[sudo] password for samedi:
Last metadata expiration check: 2:58:29 ago
<...>

[samedi@web ~]$ sudo dnf update
Extra Packages for Enterprise Linux Modular 8 - x86_64
<...>

[samedi@web ~]$ sudo dnf install -y https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Last metadata expiration check: 0:05:22
<...>

[samedi@web ~]$ dnf module list php
<...>
Rocky Linux 8 - AppStream
Name                Stream                 Profiles                                 Summary
php                 7.2 [d]                common [d], devel, minimal               PHP scripting language
php                 7.3                    common [d], devel, minimal               PHP scripting language
php                 7.4                    common [d], devel, minimal               PHP scripting language

Remi's Modular repository for Enterprise Linux 8 - x86_64
Name                Stream                 Profiles                                 Summary
php                 remi-7.2               common [d], devel, minimal               PHP scripting language
php                 remi-7.3               common [d], devel, minimal               PHP scripting language
php                 remi-7.4               common [d], devel, minimal               PHP scripting language
php                 remi-8.0               common [d], devel, minimal               PHP scripting language
php                 remi-8.1               common [d], devel, minimal               PHP scripting language

[samedi@web ~]$ sudo dnf module enable php:remi-8.1
Remi's Modular repository for Enterprise Linux 8 - x86_64
<...>

[samedi@web ~]$ dnf module list php
Last metadata expiration check: 0:04:27 ago on Wed 06 Oct 2021 03:06:07 PM CEST.
Rocky Linux 8 - AppStream
Name               Stream                    Profiles                                Summary
php                7.2 [d]                   common [d], devel, minimal              PHP scripting language
php                7.3                       common [d], devel, minimal              PHP scripting language
php                7.4                       common [d], devel, minimal              PHP scripting language

Remi's Modular repository for Enterprise Linux 8 - x86_64
Name               Stream                    Profiles                                Summary
php                remi-7.2                  common [d], devel, minimal              PHP scripting language
php                remi-7.3                  common [d], devel, minimal              PHP scripting language
php                remi-7.4                  common [d], devel, minimal              PHP scripting language
php                remi-8.0                  common [d], devel, minimal              PHP scripting language
php                remi-8.1 [e]              common [d], devel, minimal              PHP scripting language

[samedi@web ~]$ sudo dnf install -y httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype ph
p74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php
-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74
-php-bcmath php74-php-gmp
Last metadata expiration check:
<...>

[samedi@web ~]$ sudo mkdir /etc/httpd/sites-available
[samedi@web sites-available]$ sudo nano /etc/httpd/sites-available/linux.tp2.web.nextcloud

[samedi@web sites-available]$ sudo mkdir /etc/httpd/sites-enabled/
[sudo] password for samedi:
[samedi@web sites-available]$ sudo ln -s /etc/httpd/sites-available/linux.tp2.web /etc/httpd/sites-enabled/

[samedi@web sites-available]$ sudo mkdir -p /var/www/sub-domains/linux.tp2.web/html

[samedi@web /]$ sudo mkdir -p /usr/share/zoneinf
[samedi@web /]$ timedatectl
               Local time: Wed 2021-10-06 15:46:34 CEST
           Universal time: Wed 2021-10-06 13:46:34 UTC
                 RTC time: Wed 2021-10-06 13:46:34
                Time zone: Europe/Madrid (CEST, +0200)
System clock synchronized: no
              NTP service: inactive
          RTC in local TZ: no
          

[samedi@web /]$ sudo vi /etc/opt/remi/php74/php.ini

[samedi@web /]$ sudo wget https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
--2021-10-06 15:53:32--
<...>

[samedi@web /]$ sudo unzip nextcloud-21.0.1.zip
<...>

[samedi@web /]$ cd nextcloud
[samedi@web nextcloud]$ sudo cp -Rf * /var/www/sub-domains/com.linux.tp2.web/html/

[samedi@web nextcloud]$ sudo chown -Rf apache.apache /var/www/sub-domains/linux.tp2.web/html/

[samedi@web nextcloud]$ sudo firewall-cmd --add-port=80/tcp --permanent
[sudo] password for samedi:
success
[samedi@web nextcloud]$ sudo firewall-cmd --reload
success

[samedi@web nextcloud]$ systemctl restart httpd

```

[Fichier de configuration Apache](./conf/httpd2.conf)

[Fichier de configuration sites-avaliable](./conf/linux.tp2.web)

### B. Base de données

Mise en place de Maria-db : 
```
[samedi@db ~]$ sudo dnf install mariadb-server
[sudo] password for samedi:
Last metadata expiration check: 2:13:31
<...>

[samedi@db ~]$ systemctl enable mariadb
[samedi@db ~]$ systemctl start mariadb

[samedi@db ~]$ mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS REC
<...>


```
Repérage du port utilisé par Mariadb : 
```
[samedi@db ~]$ sudo ss -alnpt | grep mysql
LISTEN 0      80                 *:3306            *:*    users:(("mysqld",pid=5223,fd=21))
```

On ouvre le port associé : 
```
[samedi@db my.cnf.d]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[samedi@db my.cnf.d]$ sudo firewall-cmd --reload
success
```

Préparation de la base donnée : 

```
[samedi@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 13
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```

Depuis `web.tp2.linux` : 
```
[samedi@web /]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 12
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.001 sec)

MariaDB [(none)]> USE nextcloud;
Database changed
MariaDB [nextcloud]> SHOW TABLES;
Empty set (0.001 sec)
```

Lister les utilisateurs de la base données : 
```
MariaDB [(none)]> SELECT User, Host FROM mysql.user;
+-----------+--------------+
| User      | Host         |
+-----------+--------------+
| nextcloud | 10.102.1.11  |
| root      | 127.0.0.1    |
| root      | ::1          |
| root      | db.tp2.linux |
| root      | localhost    |
+-----------+--------------+
5 rows in set (0.001 sec)
```

### C. Finaliser l'installation de NextCloud

Modification du fichier `hosts` ma machine host : 
```
PS C:\Windows\System32\drivers\etc> cat .\hosts | Select-String -Pattern web

10.102.1.11     web.tp2.linux
```

déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation : 
```
MariaDB [(none)]> SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'nextcloud';
+----------+
| COUNT(*) |
+----------+
|       77 |
+----------+
1 row in set (0.002 sec)

```

Tableau récapitulatif : 

| Machine         | IP            | Service                 | Port ouvert   | IP autorisées |
|-----------------|---------------|-------------------------|-------------  |---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             |80/tcp 22/tcp 1234/tcp| x             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données |3306/tcp 22/tcp| `10.102.1.11` |

