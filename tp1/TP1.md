# TP1 : (re)Familiaration avec un système GNU/Linux

## 0. Préparation de la machine

Preuve que tout est fait en `ssh`: 
```
PS C:\Users\33785> ssh samedi@10.101.1.11
samedi@10.101.1.11's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Sep 22 09:44:15 2021
```

On affiche les routes avec `ip r s`:

```
[samedi@node1 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
```
On peut donc voir la présence d'une route par défault et enp0s3 étant de nom de la carte NAT dans la VM.

En faisaant un `nano` sur le fichier ifcfg-enps0s8 on peut voir la configuration de la carte : 

```
NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.101.1.11
NETMASK=255.255.255.0
DNS=1.1.1.1
```
On peut donc voir que l'on est sur une interface réseau différente de celle de la question précédente et que la VM possède une IP statique.On voit aussi que le DNS a bien été défini comme demandé.

Pour attribuer un nom à une machine il faut modifier le fichier /etc/hosts à l'aide de `nano`, on peut vérifier que les changements ont eu lieu à l'aide de la commande `hostname` : 

```
[samedi@node1 network-scripts]$ hostname
node1.tp1.b2
```

résultat du `dig` : 

```
[samedi@node1 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 21012
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4000
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               9729    IN      A       92.243.16.143

;; Query time: 11 msec
;; SERVER: 10.33.10.2#53(10.33.10.2)
;; WHEN: Wed Sep 22 13:57:21 CEST 2021
;; MSG SIZE  rcvd: 53
```
voici la ligne contenant la réponse du `dig` : 
```
;; ANSWER SECTION:
ynov.com.               9729    IN      A       92.243.16.143
```

et l'adresse du serveur qui nous a répondu : 
```
;; SERVER: 10.33.10.2#53(10.33.10.2)
```

Preuve que le fichier `hosts` a bien été édité : 

```
[samedi@node1 network-scripts]$ ping vm2
PING noed2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
64 bytes from noed2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=1.04 ms
64 bytes from noed2.tp1.b2 (10.101.1.12): icmp_seq=2 ttl=64 time=0.543 ms
^C
--- noed2.tp1.b2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.543/0.791/1.040/0.250 ms
```

On se sert de `sudo firewall-cmd --list-all` pour afficher la configuration du firewall : 
```
[samedi@node1 ~]$ sudo firewall-cmd --list-all
[sudo] password for samedi:
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```


## I. Utilisateurs

### 1. Création et configuration

Pour créer un nouvel utlisateur on utilise la commande `useradd` et utilise les paramètres `-m` pour mettre en place un répertoire personnel et `-s /bin/bash` pour définir /bin/bash comme shell

```
[samedi@node1 network-scripts]$ sudo useradd bigbrother -m -s /bin/bash
[samedi@node1 network-scripts]$
```

Pour créer un groupe on utilise `groupadd` : 
```
[samedi@node1 network-scripts]$ sudo groupadd admins
```

On utilise `usermod` pour ajouter un utilisateur dans un groupe, ensuite on peut vérifier que ça a bien fonctionné avec la commmande `groups <nom du user>` : 


```
[samedi@node1 ~]$ sudo usermod -aG admins bigbrother
[samedi@node1 ~]$ groups bigbrother
bigbrother : bigbrother admins
```


### 2. SSH

Création d'une paire de clé Ssh : 
```
PS C:\Users\33785> ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\33785/.ssh/id_rsa):
C:\Users\33785/.ssh/id_rsa already exists.
Overwrite (y/n)? y
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\33785/.ssh/id_rsa.
Your public key has been saved in C:\Users\33785/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:cn3sOCNlud7AMUda1xTA6V/i+LNWDgnYEGk9K4tJEUs 33785@LAPTOP-6AO987H5
The key's randomart image is:
+---[RSA 4096]----+
|        E..+..o.o|
|       ...+ oo o |
|        .o ++o. .|
|        ..o*+o. .|
|      ..SoOo++.o.|
|       oo+.O. +..|
|        . B .. + |
|         o =  + .|
|          . ...o |
+----[SHA256]-----+
```

test de connexion ssh : 
```
PS C:\Users\33785> ssh samedi@10.101.1.11
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Thu Sep 23 09:35:20 2021 from 10.101.1.1
```

## II. Partitionnement

### 2. Partitionnement

On commence par repérer les nouveaux disques à l'aide `lsblk` : 
```
[samedi@node1 ~]$ lsblk
NAME                        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda                           8:0    0    8G  0 disk
├─sda1                        8:1    0    1G  0 part /boot
└─sda2                        8:2    0    7G  0 part
  ├─rl_bastion--ovh1fr-root 253:0    0  6.2G  0 lvm  /
  └─rl_bastion--ovh1fr-swap 253:1    0  820M  0 lvm  [SWAP]
sdb                           8:16   0    3G  0 disk
sdc                           8:32   0    3G  0 disk
sr0                          11:0    1 1024M  0 rom
```

On a bien `sdb` et `sbc` deux volumes physiques de 3 Go chacuns.

Pour ajouter des disques dans LVM on utilise `pvcreate` : 
```
[samedi@node1 dev]$ sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
[samedi@node1 dev]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
```

Pour les agréger les deux en un seuk volume group on commence par créer le volume group à l'aide de `vgcreate`, on pourra ensuite ajouter le second disque dur au volume group à l'aide `vgextend` : 
```
[samedi@node1 dev]$ sudo vgcreate grouptp /dev/sdb | sudo vgextend grouptp /dev/sdc
[sudo] password for samedi:
  Volume group "grouptp" not found
  Cannot process volume group grouptp
[samedi@node1 dev]$ sudo vgs
  VG                #PV #LV #SN Attr   VSize  VFree
  grouptp             1   0   0 wz--n- <3.00g <3.00g
  rl_bastion-ovh1fr   1   2   0 wz--n- <7.00g     0
```
J'ai voulu enchaîner les 2 commandes d'un coup, ça n'a pas marché cependant comme le volume group a bien été créé je vais juste réessayer d'ajouter le second disque dur : 
```
[samedi@node1 dev]$ sudo vgextend grouptp /dev/sdc
  Volume group "grouptp" successfully extended
[samedi@node1 dev]$ sudo vgs
[sudo] password for samedi:
  VG                #PV #LV #SN Attr   VSize  VFree
  grouptp             2   0   0 wz--n-  5.99g 5.99g
  rl_bastion-ovh1fr   1   2   0 wz--n- <7.00g    0
```
Au final j'ai bien pu ajouté le disque dur au volume group.

Pour créer un logical volume on utilise `lvcreate` : 
```
[samedi@node1 dev]$ sudo lvcreate -L 1G grouptp -n audi
[sudo] password for samedi:
  Logical volume "audi" created.
[samedi@node1 dev]$ sudo lvcreate -L 1G grouptp -n bmw
  Logical volume "bmw" created.
[samedi@node1 dev]$ sudo lvcreate -L 1G grouptp -n mercedes
  Logical volume "mercedes" created.
[samedi@node1 dev]$
```

Pour formater une partition on se sert de `mkfs` : 
```
sudo mkfs -t ext4 /dev/grouptp/audi
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 53db8633-0293-446b-ba28-3b6f4a3becf8
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
<...>
```

Pour monter une partition on commence par créer un dossier à l'aide de `mkdir` dans lequel on va ensuite pouvoir monter la partition à l'aide `mount` : 
```
[samedi@node1 dev]$ sudo mkdir /mnt/part1
[samedi@node1 dev]$ sudo mount /dev/grouptp/audi /mnt/part1
<..>*.>
```
On peut maintenant aller dans les partitions depuis `/mnt` : 
```
[samedi@node1 dev]$ cd /mnt
[samedi@node1 mnt]$ ls
part1  part2  part3
[samedi@node1 mnt]$ cd part1
[samedi@node1 part1]$
```

Pour faire en sorte que ces partitions soient autmatiquement montées au démarrage il faut aller modifier le fichier de configuration dans `/etc/fstab` comme montré si-dessous : 
```
/dev/grouptp/audi /mnt/part1                                ext4    defaults        0 0
```

## III. Gestion de services

## 1. Interaction avec un service existant

Pour vérifier qu'une unité est active on utilise `systemctl is-active` et pour vérifier qu'elle est activée on utilise `systemctl is-enabled` : 
```
[samedi@node1 mnt]$ systemctl is-active firewalld
active
[samedi@node1 mnt]$ systemctl is-enabled firewalld
enabled
```
## 2. Création de service

### A. Unité simpliste

On commence par créer un fichier `web.service` dans `/etc/systemd/system` à l'aide `nano`, une fois cela fait on peut faire `sudo systemctl daemon-reload` pour que soit pris en compte le nouveau service : 
```
[samedi@node1 system]$ sudo nano
[samedi@node1 system]$ [samedi@node1 system]$
[samedi@node1 system]$ ls
basic.target.wants                          
<...>
web.service
[samedi@node1 ~]$ sudo systemctl daemon-reload
[sudo] password for samedi:
```

On ouvre le port 8888 à l'aide de `firewall-cmd` pour que le service puisse fonctionner : 
```
[samedi@node1 ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
[samedi@node1 system]$ sudo firewall-cmd --reload
success
```

On peut enfin voir que le service démarre grâce à `systemctl status` et que l'on peut accéder au serveur web grâce à `curl` : 
```
[samedi@node1 system]$ sudo systemctl status web
[sudo] password for samedi:
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-09-23 10:36:04 CEST; 42min ago
 Main PID: 27526 (python3)
    Tasks: 1 (limit: 11398)
   Memory: 9.6M
   CGroup: /system.slice/web.service
           └─27526 /bin/python3 -m http.server 8888

Sep 23 10:36:04 node1.tp1.b2 systemd[1]: Started Very simple web service.
[samedi@node1 system]$
```

`curl` fait depuis la machine hôte : 
```
PS C:\Users\33785> curl 10.101.1.11:8888


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                    <html>
                    <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                    <title>Directory listing fo...
RawContent        : HTTP/1.0 200 OK
                    Content-Length: 942
<...>
```

### B. Modification de l'unité

Création de l'utilisateur `web` : 
```
[samedi@node1 .ssh]$ sudo useradd web -m -s /bin/bash
[samedi@node1 .ssh]$ passwd web
passwd: Only root can specify a user name.
[samedi@node1 .ssh]$ sudo !!
sudo passwd web
Changing password for user web.
New password:
BAD PASSWORD: The password is a palindrome
Retype new password:
passwd: all authentication tokens updated successfully.
 
```

`nano` de la configuration de `web.service` : 
```
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888
User=web
Workingdirectoty= /srv/

[Install]
WantedBy=multi-user.target

```

`curl` depuis la machine hôte : 
```
PS C:\Users\33785> curl 10.101.1.11:8888                                                                                

StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                    <html>
                    <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                    <title>Directory listing fo...
RawContent        : HTTP/1.0 200 OK
                    Content-Length: 942
                    Content-Type: text/html; charset=utf-8
                    Date: Thu, 23 Sep 2021 10:32:15 GMT
                    Server: SimpleHTTP/0.6 Python/3.6.8

                    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//...
<...>
```