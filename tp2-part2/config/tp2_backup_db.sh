#!/bin/bash
# simple mysql db backup
# Fabio - 22/10/2021

  backup_name=$(date +hello_%y%m%d_%H%M%S.tar.gz)
  backup_fullpath="$(pwd)/${backup_name}"

  mysqldump --user=nextcloud --password=meow --databases $2 > tmp_backup.sql

  tar cvzf $backup_name tmp_backup.sql

  rsync -av --remove-source-files $backup_fullpath $1

  rm -rf tmp_backup.sql
