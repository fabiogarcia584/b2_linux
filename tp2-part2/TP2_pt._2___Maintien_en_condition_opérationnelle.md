# TP2 pt. 2 : Maintien en condition opérationnelle

# Sommaire

- [TP2 pt. 2 : Maintien en condition opérationnelle](#tp2-pt-2--maintien-en-condition-opérationnelle)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. Monitoring](#i-monitoring)
  - [1. Le concept](#1-le-concept)
  - [2. Setup](#2-setup)
- [II. Backup](#ii-backup)
  - [1. Intwo bwo](#1-intwo-bwo)
  - [2. Partage NFS](#2-partage-nfs)
  - [3. Backup de fichiers](#3-backup-de-fichiers)
  - [4. Unité de service](#4-unité-de-service)
    - [A. Unité de service](#a-unité-de-service)
    - [B. Timer](#b-timer)
    - [C. Contexte](#c-contexte)
  - [5. Backup de base de données](#5-backup-de-base-de-données)
  - [6. Petit point sur la backup](#6-petit-point-sur-la-backup)
- [III. Reverse Proxy](#iii-reverse-proxy)
  - [1. Introooooo](#1-introooooo)
  - [2. Setup simple](#2-setup-simple)
  - [3. Bonus HTTPS](#3-bonus-https)
- [IV. Firewalling](#iv-firewalling)
  - [1. Présentation de la syntaxe](#1-présentation-de-la-syntaxe)
  - [2. Mise en place](#2-mise-en-place)
    - [A. Base de données](#a-base-de-données)
    - [B. Serveur Web](#b-serveur-web)
    - [C. Serveur de backup](#c-serveur-de-backup)
    - [D. Reverse Proxy](#d-reverse-proxy)
    - [E. Tableau récap](#e-tableau-récap)

# I. Monitoring


## 2. Setup

🌞 **Setup Netdata**

Installation de `netdata` : 

```bash=
[samedi@web ~]$ sudo su -
[sudo] password for samedi:
[root@web ~]# bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
<...>
[root@web ~]# exit
logout
```


🌞 **Manipulation du *service* Netdata**


Vérification de l'état du service : 

```bash=
[samedi@web ~]$ systemctl is-active netdata
active
[samedi@web ~]$ systemctl is-enabled netdata
enabled
```

Configuration du firewall : 

```bash=
[samedi@web ~]$ sudo ss -alnpt | grep netdata
[sudo] password for samedi:
LISTEN 0      128          0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=2274,fd=5))                            
LISTEN 0      128        127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=2274,fd=35))                           
LISTEN 0      128             [::]:19999         [::]:*    users:(("netdata",pid=2274,fd=6))                            
LISTEN 0      128            [::1]:8125          [::]:*    users:(("netdata",pid=2274,fd=34))  

[samedi@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
[sudo] password for samedi:
success
[samedi@web ~]$ sudo firewall-cmd --add-port=8125/tcp --permanent
success
[samedi@web ~]$ sudo firewall-cmd --reload
success
```


🌞 **Setup Alerting**

Mise en place des alertes discord : 

   Ajout du lien du webhook discord dans la config de netdata : 
```bash=
[samedi@web netdata]$ ./edit-config health_alarm_notify.conf
Editing '/opt/netdata/etc/netdata/health_alarm_notify.conf' ...
```

   Vérification du bon fonctionnement des alertes : 
   
```bash=
[samedi@web netdata]$ sudo su -s /bin/bash netdata
[sudo] password for samedi:
bash-4.4$ /opt/netdata/etc/netdata/custom-plugins.d/alarm-notify.sh test
bash: /opt/netdata/etc/netdata/custom-plugins.d/alarm-notify.sh: No such file or directory
bash-4.4$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test

# SENDING TEST WARNING ALARM TO ROLE: sysadmin
2021-10-11 14:39:23: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is WARNING to 'alarms'
# OK
<...>
```
Après avoir tapé la commande j'ai bien reçu 3 alertes sur mon serveur discord.


🌞 **Config alerting**

Création d'une alerte pour la ram : 

```bash=
[samedi@web ~]$ cd /opt/netdata/etc/netdata/
[samedi@web netdata]$ sudo touch health.d/ram-usage.conf
[sudo] password for samedi:
[samedi@web netdata]$ sudo ./edit-config health.d/ram-usage.conf
Editing '/opt/netdata/etc/netdata/health.d/ram-usage.conf' ...
```

Fihchier d'alerte : 

```bash=
[samedi@web netdata]$ cat health.d/ram-usage.conf
larm: ram_usage
    on: system.ram
lookup: average -1m percentage of used
 units: %
 every: 1m
  warn: $this > 50
  crit: $this > 60
  info: The percentage of RAM being used by the system.
```

Installation de `stress` : 

```bash=
[samedi@web ~]$ sudo dnf install -y stress
Last metadata expiration check: 4:43:24
<...>
```

Stress-test de la ram :

```bash=
[samedi@web ~]$ stress --vm 1 --vm-bytes 1224M
stress: info: [4132] dispatching hogs: 0 cpu, 0 io, 1 vm, 0 hdd
```

Message reçu sur Discord : 

```bash=
web.tp2.linux needs attention, system.ram (ram), ram usage = 50.3%
```


# II. Backup

## 2. Partage NFS

🌞 **Setup environnement**

Création des fichers de backups : 
```bash=
[samedi@backup ~]$ sudo mkdir -p /srv/backups/
[sudo] password for samedi:
[samedi@backup ~]$ cd /srv/backups/
[samedi@backup backups]$ sudo mkdir {web,db}.tp2.linux
[samedi@backup backups]$ ls
db.tp2.linux  web.tp2.linux
```

🌞 **Setup partage NFS**

Installation de `nfs-utils` : 

```bash=
[samedi@backup ~]$ sudo dnf -y install nfs-utils
[sudo] password for samedi:
Last metadata expiration check: 1 day, 2:37:41 ago on Mon 11 Oct 2021 12:00:27 PM CEST.
Dependencies resolved.
<...>
```

Configuration du serveur nfs : 
```bash=
[samedi@backup ~]$ cat /etc/exports
/srv/backups/web.tp2.linux    10.102.1.11(rw,sync,no_root_squash)
/srv/backups/db.tp2.linux    10.102.1.12(rw,sync,no_root_squash)
[samedi@backup ~]$ sudo systemctl enable --now rpcbind nfs-server
[samedi@backup ~]$ sudo firewall-cmd --add-service={nfs3,mountd,rpc-bind} --permanent
success
[samedi@backup ~]$ sudo firewall-cmd --reload
[sudo] password for samedi:
success
```

🌞 **Setup points de montage sur `web.tp2.linux`**

```bash=
[samedi@web ~]$ sudo dnf -y install nfs-utils
[sudo] password for samedi:
Last metadata expiration
[samedi@web /]$ cat /etc/idmapd.conf | grep Domain
Domain = tp2.linux

[samedi@web /]$ sudo mount -t nfs backup.tp2.linux:/srv/backups/web.tp2.linux /srv/backup/

[samedi@web /]$ mount | grep backup.tp2.linux
backup.tp2.linux:/srv/backups/web.tp2.linux on /srv/backup type nfs4 (rw,relatime,vers=4.2,rsize=262144,wsize=262144,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=10.102.1.11,local_lock=none,addr=10.102.1.13)

[samedi@web /]$ df -h | grep backup.tp2.linux
backup.tp2.linux:/srv/backups/web.tp2.linux  6.2G  3.9G  2.4G  63% /srv/backup

[samedi@web backup]$ sudo touch /srv/backup/test
[samedi@web backup]$ ls
test

[samedi@web srv]$ cat /etc/fstab | grep backup.tp2
backup.tp2.linux:/srv/backups/web.tp2.linux /srv/backup     nfs     defaults        0 0
```


🌟 **BONUS** : partitionnement avec LVM


## 3. Backup de fichiers

🌞 **Rédiger le script de backup `/srv/tp2_backup.sh`**

[tp2_backup.sh](config/tp2_backup.sh)


🌞 **Tester le bon fonctionnement**

```bash=
[samedi@web srv]$ ls
backup  tp2_backup.sh
[samedi@web srv]$ sudo ./tp2_backup.sh /home/samedi/scripts /etc/sysconfig/network-scripts/
tar: Removing leading '/' from member names
drwxrwxr-x              6 2021/10/12 17:52:41 scripts
[samedi@web srv]$ ls
backup  tp2_backup_211012_184647.tar.gz  tp2_backup.sh
[samedi@web srv]$ zcat tp2_backup_211012_184647.tar.gz
etc/sysconfig/network-scripts/0000755000000000000000000000000014104532154015510 5ustar  rootrootetc/sysconfig/network-scripts/ifcfg-enp0s30000644000000000000000000000040414122614757017627 0ustar  rootrootTYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=dhcp
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
<...>
```

On peut donc voir que le script a bien archivé les fichiers que je lui ai demandé.


## 4. Unité de service

### A. Unité de service

🌞 **Créer une *unité de service*** pour notre backup

```bash=
[samedi@web ~]$ sudo touch /etc/systemd/system/tp2_backup.service
[sudo] password for samedi:
[samedi@web ~]$ sudo vi /etc/systemd/system/tp2_backup.service
[samedi@web ~]$ cat /etc/systemd/system/tp2_backup.service
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh /home/samedi/scripts /etc/sysconfig/network-scripts/ifcfg-enp0s8
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```


🌞 **Tester le bon fonctionnement**

```bash=
[samedi@web scripts]$ ls
[samedi@web scripts]$ sudo systemctl daemon-reload
[samedi@web scripts]$ sudo systemctl start tp2_backup.service
[samedi@web scripts]$ ls
hello_211012_235238.tar.gz
```

### B. Timer

Un *timer systemd* permet l'exécution d'un *service* à intervalles réguliers.

🌞 **Créer le *timer* associé à notre `tp2_backup.service`**


```bash=
[samedi@web ~]$ cd /etc/systemd/system/
[samedi@web system]$ sudo touch tp2_backup.timer && sudo vi tp2_backup.timer
[sudo] password for samedi:
[samedi@web system]$ sudo cat tp2_backup.timer
t]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* *:*:00

[Install]
WantedBy=timers.target
```


🌞 **Activez le timer**

```bash=
[samedi@web system]$ sudo systemctl start tp2_backup.timer
[sudo] password for samedi:

[samedi@web system]$ sudo systemctl enable tp2_backup.timer
Created symlink /etc/systemd/system/timers.target.wants/tp2_backup.timer → /etc/systemd/system/tp2_backup.timer.

[samedi@web system]$ sudo systemctl status tp2_backup.timer
● tp2_backup.timer - Periodically run our TP2 backup script
   Loaded: loaded (/etc/systemd/system/tp2_backup.timer; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-10-13 01:46:30 CEST; 4min 31s ago
  Trigger: n/a

```

🌞 **Tests !**
- avec la ligne `OnCalendar=*-*-* *:*:00`, le *timer* déclenche l'exécution du *service* toutes les minutes
- vérifiez que la backup s'exécute correctement

```bash=
[samedi@web scripts]$ ls
hello_211013_014800.tar.gz  hello_211013_014902.tar.gz  hello_211013_015000.tar.gz  hello_211013_015101.tar.gz  hello_211013_015200.tar.gz
```

On peut bien voir dans les noms de fichiers que les backups sont effectuées avec 1 minute d'intervalle.

### C. Contexte

🌞 **Faites en sorte que...**
- votre backup s'exécute sur la machine `web.tp2.linux`
- le dossier sauvegardé est celui qui contient le site NextCloud (quelque part dans `/var/`)
- la destination est le dossier NFS monté depuis le serveur `backup.tp2.linux`
- la sauvegarde s'exécute tous les jours à 03h15 du matin
- prouvez avec la commande `sudo systemctl list-timers` que votre *service* va bien s'exécuter la prochaine fois qu'il sera 03h15


```bash=
[samedi@web ~]$ sudo vi /etc/systemd/system/tp2_backup.service
[sudo] password for samedi:
[samedi@web ~]$ sudo cat /etc/systemd/system/tp2_backup.service |grep ExecStart=
ExecStart=/srv/tp2_backup.sh /srv/backup/ /var/www/sub-domains

[samedi@web /]$ sudo vi /etc/systemd/system/tp2_backup.timer
[samedi@web /]$ sudo cat /etc/systemd/system/tp2_backup.timer |grep OnCalendar
OnCalendar=*-*-* 3:15:00
[samedi@web /]$ sudo systemctl daemon-reload

[samedi@web /]$ sudo systemctl list-timers |grep  tp2_backup.timer
Mon 2021-10-25 03:15:00 CEST  3h 43min left Sun 2021-10-24 23:30:03 CEST  1min 5s ago  tp2_backup.timer             tp2_backup.service
[samedi@web /]$
```

[tp2_backup.timer](config/tp2_backup.timer)
[tp2_backup.service](config/tp2_backup.service)
## 5. Backup de base de données

Sauvegarder des dossiers c'est bien. Mais sauvegarder aussi les bases de données c'est mieux.

🌞 **Création d'un script `/srv/tp2_backup_db.sh`**

- il utilise la commande `mysqldump` pour récupérer les données de la base de données
- cela génère un fichier `.sql` qui doit ensuite être compressé en `.tar.gz`
- il s'exécute sur la machine `db.tp2.linux`
- il s'utilise de la façon suivante : 

 ```bash
$ ./tp2_backup_db.sh <DESTINATION> <DATABASE>
```
[tp2_backup_db.sh](config/tp2_backup_db.sh)

🌞 **Restauration**

- tester la restauration de données
- c'est à dire, une fois la sauvegarde effectuée, et le `tar.gz` en votre possession, tester que vous êtes capables de restaurer la base dans l'état au moment de la sauvegarde
  - il faut réinjecter le fichier `.sql` dans la base à l'aide d'une commmande `mysql`

impossible de se connecter à la base de donnée j'ai une erreur à l'exécution de mon script : 
```bash=
+ mysqldump --user=nextcloud --password=meow --databases nextcloud
mysqldump: Got error: 1045: "Access denied for user 'nextcloud'@'localhost' (using password: YES)" when trying to connect
```

🌞 ***Unité de service***

- pareil que pour la sauvegarde des fichiers ! On va faire de ce script une *unité de service*.
- votre script `/srv/tp2_backup_db.sh` doit pouvoir se lancer grâce à un *service* `tp2_backup_db.service`
- le *service* est exécuté tous les jours à 03h30 grâce au *timer* `tp2_backup_db.timer`
- prouvez le bon fonctionnement du *service* ET du *timer*

```bash=
[samedi@db srv]$ sudo systemctl is-enabled tp2_backup_db.timer
enabled
[samedi@db srv]$ sudo systemctl is-active tp2_backup_db.timer
active
[samedi@db srv]$ sudo systemctl list-timers
NEXT                          LEFT          LAST                          PASSED    UNIT                         ACTIVATES
Mon 2021-10-25 00:47:23 CEST  31min left    Sun 2021-10-24 23:30:23 CEST  45min ago dnf-makecache.timer          dnf-makecache.service
Mon 2021-10-25 03:30:00 CEST  3h 14min left n/a                           n/a       tp2_backup_db.timer          tp2_backup_db.service
Mon 2021-10-25 23:31:18 CEST  23h left      Sun 2021-10-24 23:31:18 CEST  44min ago systemd-tmpfiles-clean.timer systemd-tmpfiles-clean.service
```
[tp2_backup_db.timer](tp2_backup_db.timer)
[tp2_backup_db.service](tp2_backup_db.service)
# III. Reverse Proxy

## 2. Setup simple

🌞 **Installer NGINX**

`epel-release` a déjà été installé dans mon patron car j'en ai eu besoin quand on a du faire des stress-test.
```bash=
[samedi@front ~]$ sudo dnf install -y nginx
[sudo] password for samedi:
Rocky Linux 8 - AppStream
[samedi@front ~]$ nginx -v
nginx version: nginx/1.14.1
```

🌞 **Tester !**

```bash=
[samedi@front ~]$ sudo systemctl start nginx
[samedi@front ~]$ sudo systemctl enable nginx

[samedi@front ~]$ cat /etc/nginx/nginx.conf |grep listen
        listen       80 default_server;
        listen       [::]:80 default_server;
        
```

fait depuis mon pc portable : 
```bash=
PS C:\Users\33785> curl 10.102.1.14:80 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
    <...>
```

🌞 **Explorer la conf par défaut de NGINX**

```bash=
[samedi@front ~]$ cat /etc/nginx/nginx.conf |grep user
user nginx;
<...>

[samedi@front ~]$ cat /etc/nginx/nginx.conf |grep server
    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;

[samedi@front ~]$ cat /etc/nginx/nginx.conf |grep include
include /usr/share/nginx/modules/*.conf;
    include             /etc/nginx/mime.types;
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    include /etc/nginx/conf.d/*.conf;
        include /etc/nginx/default.d/*.conf;
#        include /etc/nginx/default.d/*front
```

🌞 **Modifier la conf de NGINX**

```bash=
[samedi@front ~]$ sudo vi /etc/nginx/nginx.conf
[samedi@front ~]$ sudo touch /etc/nginx/conf.d/web.tp2.linux.conf
[samedi@front ~]$ sudo vi /etc/nginx/conf.d/web.tp2.linux.conf
[samedi@front ~]$ cat /etc/nginx/conf.d/web.tp2.linux.conf
server {
    listen 80;

    server_name web.tp2.linux;

      location / {
              proxy_pass http://web.tp2.linux;
    }
}
```

# IV. Firewalling

## 2. Mise en place

### A. Base de données

🌞 **Restreindre l'accès à la base de données `db.tp2.linux`**

- seul le serveur Web doit pouvoir joindre la base de données sur le port 3306/tcp
- vous devez aussi autoriser votre accès SSH
- n'hésitez pas à multiplier les zones (une zone `ssh` et une zone `db` par exemple)


```bash=
[samedi@db ~]$ sudo firewall-cmd --set-default-zone=drop
[sudo] password for samedi:
success
[samedi@db ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8
Warning: ZONE_ALREADY_SET: 'enp0s8' already bound to 'drop'
success
[samedi@db ~]$ sudo firewall-cmd --new-zone=ssh --permanent
success
[samedi@db ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
success
[samedi@db ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
success
[samedi@db ~]$ sudo firewall-cmd --new-zone=db --permanent
success
[samedi@db ~]$ sudo firewall-cmd --zone=db --add-source=10.102.1.11/32 --permanent
success
[samedi@db ~]$ sudo firewall-cmd --zone=db --add-port=3306/tcp --permanent
success
[samedi@db ~]$ sudo firewall-cmd --reload
success
[samedi@db ~]$
```

🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

```bash=
[samedi@db ~]$ sudo firewall-cmd --get-active-zones
db
  sources: 10.102.1.11/32
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/32
[samedi@db ~]$ sudo firewall-cmd --list-all --zone=db
db (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.11/32
  services:
  ports: 3306/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[samedi@db ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[samedi@db ~]$
```

### B. Serveur Web

🌞 **Restreindre l'accès au serveur Web `web.tp2.linux`**

- seul le reverse proxy `front.tp2.linux` doit accéder au serveur web sur le port 80
- n'oubliez pas votre accès SSH


```bash=
[samedi@web ~]$ sudo firewall-cmd --set-default-zone=drop
[sudo] password for samedi:
success
[samedi@web ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8
Warning: ZONE_ALREADY_SET: 'enp0s8' already bound to 'drop'
success
[samedi@web ~]$ sudo firewall-cmd --new-zone=ssh --permanent
success
[samedi@web ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
success
[samedi@web ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
success
[samedi@web ~]$ sudo firewall-cmd --new-zone=proxy --permanent
success
[samedi@web ~]$ sudo firewall-cmd --zone=proxy --add-source=10.102.1.14/32 --permanent
success
[samedi@web ~]$ sudo firewall-cmd --zone=proxy --add-port=80/tcp --permanent
success
[samedi@web ~]$ sudo firewall-cmd --reload
success
[samedi@web ~]$
```

🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

```bash=
[samedi@web ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
proxy
  sources: 10.102.1.14/32
ssh
  sources: 10.102.1.1/32
[samedi@web ~]$ sudo firewall-cmd --get-default-zone
drop
[samedi@web ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[samedi@web ~]$ sudo firewall-cmd --list-all --zone=proxy
proxy (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.14/32
  services:
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[samedi@web ~]$
```

### C. Serveur de backup

🌞 **Restreindre l'accès au serveur de backup `backup.tp2.linux`**

- seules les machines qui effectuent des backups doivent être autorisées à contacter le serveur de backup *via* NFS
- n'oubliez pas votre accès SSH

```bash=
[samedi@backup ~]$ sudo firewall-cmd --set-default-zone=drop
[sudo] password for samedi:
success
[samedi@backup ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8
Warning: ZONE_ALREADY_SET: 'enp0s8' already bound to 'drop'
success
[samedi@backup ~]$ sudo firewall-cmd --new-zone=ssh --permanent
success
[samedi@backup ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
success
[samedi@backup ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
success
[samedi@backup ~]$ sudo firewall-cmd --new-zone=backup --permanent
success
[samedi@backup ~]$ sudo firewall-cmd --zone=backup --add-source=10.102.1.12/32 --add-source=10.102.1.11/32 --permanent
success
[samedi@backup ~]$ sudo firewall-cmd --zone=backup --add-port=111/udp --add-port=111/tcp --add-port=2049/udp --add-port=2049/tcp --permanent
success
[samedi@backup ~]$ sudo firewall-cmd --reload
success
[samedi@backup ~]$
```

🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

```bash=
[samedi@backup ~]$ sudo firewall-cmd --get-active-zones
backup
  sources: 10.102.1.12/32 10.102.1.11/32
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/32
[samedi@backup ~]$ sudo firewall-cmd --get-default-zone
drop
[samedi@backup ~]$ sudo firewall-cmd --info-zone=ssh
^[[Assh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[samedi@backup ~]$ sudo firewall-cmd --info-zone=backup
backup (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.12/32 10.102.1.11/32
  services:
  ports: 111/udp 2049/udp 2049/tcp 111/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[samedi@backup ~]$
```

🌞 **Restreindre l'accès au reverse proxy `front.tp2.linux`**

- seules les machines du réseau `10.102.1.0/24` doivent pouvoir joindre le proxy
- n'oubliez pas votre accès SSH

```bash=
[samedi@front ~]$ sudo firewall-cmd --set-default-zone=drop
[sudo] password for samedi:
success
[samedi@front ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8
Warning: ZONE_ALREADY_SET: 'enp0s8' already bound to 'drop'
success
[samedi@front ~]$ sudo firewall-cmd --new-zone=ssh --permanent
success
[samedi@front ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
success
[samedi@front ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
success
[samedi@front ~]$ sudo firewall-cmd --new-zone=proxy --permanent
success
[samedi@front ~]$ sudo firewall-cmd --zone=proxy --add-source=10.102.1.0/24 --permanent
success
[samedi@front ~]$ sudo firewall-cmd --zone=proxy --add-port=80/tcp --permanent
success
[samedi@front ~]$ sudo firewall-cmd --reload
success
[samedi@front ~]$
```

🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

```bash=
[samedi@front ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
proxy
  sources: 10.102.1.0/24
ssh
  sources: 10.102.1.1/32
[samedi@front ~]$ sudo firewall-cmd --get-default-zone
drop
[samedi@front ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[samedi@front ~]$ sudo firewall-cmd --list-all --zone=proxy
proxy (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.0/24
  services:
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[samedi@front ~]$
```

### E. Tableau récap

🌞 **Rendez-moi le tableau suivant, correctement rempli :**


| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             |80/tcp       |10.102.1.14/32 |
|                    | `10.102.1.11` | Serveur Web             |22/tcp       |10.102.1.1/32  |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données |3306/tcp     |10.102.1.11/32 |
|                    | `10.102.1.12` | Serveur Base de Données |22/tcp       |10.102.1.1/32  |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) |111/tcp 2049/tcp 111/udp 2049/udp |10.102.1.11/32 |
|                    | `10.102.1.13` | Serveur de Backup (NFS) |111/tcp 2049/tcp 111/udp 2049/udp |10.102.1.12/32 |
|                    | `10.102.1.13` | Serveur de Backup (NFS) |22/tcp       |10.102.1.1/32  |
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           |80/tcp       |10.102.1.0/24  |
|                    | `10.102.1.14` | Reverse Proxy           |22/tcp       |10.102.1.1/32  |

